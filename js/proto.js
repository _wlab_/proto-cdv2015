$(function() {

  console.log('"Conscient de l\'aveuglante lumiere qui lui jaillissait d\'une main, il usait de l\'autre pour leur cacher les yeux."');

  var DEFAULT_SPEED = 0.5;

  var renderer, scene, camera, controls, stats;

  var CharacterFigure, character,
      characterContainer, characterMesh, characterGeom,
      characterMaterial, characterTexture,
      characterWidth = 674 / 2,
      characterHeight = 195 / 2;

  var SleeveFigure, sleeves,
      sleeveGroup = new THREE.Group(),
      sleeveMaterial, sleeveTexture,
      sleeveWidth = 512,
      sleeveHeight = 256,
      sleeveHeightHalf = 256 / 2,
      sleeveGeometry = new THREE.PlaneBufferGeometry(sleeveWidth, sleeveHeight);

  var BuildingFigure, building,
      buildingContainer01, buildingContainer02, buildingContainer03,
      buildingGroup = new THREE.Group(),
      buildingMaterial01, buildingMaterial02, buildingMaterial03,
      buildingTexture01, buildingTexture02, buildingTexture03,
      buildingPart01Width = 133,
      buildingPart01Height = 244,
      buildingPart02Width = 285,
      buildingPart02Height = 244,
      buildingPart03Width = 122,
      buildingPart03Height = 244;

  var LightPoles, lightPoles,
      lightPoleMaterial, lightPoleTexture, lightPoleGeom,
      lightPole01Mesh, lightPole02Mesh,
      lightPole01Container, lightPole02Container;
      lightPoleWidth = 75 / 2,
      lightPoleHeight = 234 / 2;

  var hemiLight = new THREE.HemisphereLight(0xccddff, 0x223322, 0.5),
      ambientLight = new THREE.AmbientLight(0x202020);

  init();
  animate();

  function init() {
    stats = initStats(),
    renderer = new THREE.WebGLRenderer({ antiAlias: true });
    renderer.setClearColor(0xCCCCCC, 1);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMapEnabled = true;
    $('#webgl').append(renderer.domElement);

    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.set(150, 450, 450);

    //scene.add(new THREE.AxisHelper(100));

    // Cards sleeves.
    sleeveTexture = new THREE.ImageUtils.loadTexture('images/card_template_sleeve.png', {}, function() {
      sleeveMaterial = new THREE.MeshBasicMaterial();
      sleeveMaterial.map = sleeveTexture;
      sleeveMaterial.side = THREE.DoubleSide;

      var bottomSleeveContainer = new THREE.Object3D(),
          bottomSleeve = new THREE.Mesh(sleeveGeometry, sleeveMaterial);
      bottomSleeve.position.z = sleeveHeightHalf;
      bottomSleeve.rotation.x = 90 * Math.PI / 180;
      bottomSleeveContainer.add(bottomSleeve);
      sleeveGroup.add(bottomSleeveContainer);

      var topSleeveContainer = new THREE.Object3D(),
          topSleeve = new THREE.Mesh(sleeveGeometry, sleeveMaterial);
      topSleeve.position.y = sleeveHeightHalf;
      topSleeveContainer.add(topSleeve);
      topSleeveContainer.rotation.x = -5 * Math.PI / 180;
      sleeveGroup.add(topSleeveContainer);
      scene.add(sleeveGroup);

      // Package all in a manipulable figure.
      SleeveFigure = function(sleeves) {
        this.isOpen = true;
        this.lockTransform = false;
        this.container = sleeves;
        this.sleeves = {
          top: sleeves.children[1],
          bottom: sleeves.children[0]
        }
      };

      SleeveFigure.prototype.open = function(speed, callback) {
        if (this.lockTransform || this.isOpen) return;
        this.lockTransform = true;

        var self = this;
        if (!speed) speed = DEFAULT_SPEED;
        var targetValue = -5 * Math.PI / 180;
        TweenMax.to(this.sleeves.top.rotation, speed, { x: targetValue, onComplete:
          function completeHandler() {
            self.isOpen = true;
            self.lockTransform = false;
            if (callback) callback();
          }
        });
      };

      SleeveFigure.prototype.close = function(speed, callback) {
        if (this.lockTransform || !this.isOpen) return;
        this.lockTransform = true;

        var self = this;
        if (!speed) speed = DEFAULT_SPEED;
        var targetValue = 89 * Math.PI / 180;
        TweenMax.to(this.sleeves.top.rotation, speed, { delay: 0.4, x: targetValue, onComplete:
          function completeHandler() {
            self.isOpen = false;
            self.lockTransform = false;
            if (callback) callback();
          }
        });
      };

      sleeves = new SleeveFigure(sleeveGroup);
    });

    // Building.
    // Part 1 of 3.
    buildingTexture01 = new THREE.ImageUtils.loadTexture('images/building_plane_01.png', {}, function() {
      buildingMaterial01 = new THREE.MeshBasicMaterial(0xFFFFFF, 0);
      buildingMaterial01.map = buildingTexture01;
      buildingMaterial01.side = THREE.DoubleSide;
      buildingMaterial01.transparent = true;

      buildingContainer01 = new THREE.Object3D();

      var buildingGeom01 = new THREE.PlaneBufferGeometry(buildingPart01Width, buildingPart01Height),
          buildingPart01 = new THREE.Mesh(buildingGeom01, buildingMaterial01);
      buildingPart01.position.y = buildingPart01Height / 2;

      buildingContainer01.add(buildingPart01);
      buildingContainer01.position.x = -195;
      buildingContainer01.rotation.y = -25 * Math.PI / 180;
      buildingContainer01.position.z = 8;
      buildingGroup.add(buildingContainer01);
    });

    // Part 2 of 3.
    buildingTexture02 = new THREE.ImageUtils.loadTexture('images/building_plane_02.png', {}, function() {
      buildingMaterial02 = new THREE.MeshBasicMaterial(0xFFFFFF, 0);
      buildingMaterial02.map = buildingTexture02;
      buildingMaterial02.side = THREE.DoubleSide;
      buildingMaterial02.transparent = true;

      buildingContainer02 = new THREE.Object3D();

      var buildingGeom02 = new THREE.PlaneBufferGeometry(buildingPart02Width, buildingPart02Height),
          buildingPart02 = new THREE.Mesh(buildingGeom02, buildingMaterial02);
      buildingPart02.position.y = buildingPart02Height / 2;

      buildingContainer02.add(buildingPart02);
      buildingContainer02.rotation.y = 15 * Math.PI / 180;
      buildingGroup.add(buildingContainer02);
    });

    // Part 3 of 3.
    buildingTexture03 = new THREE.ImageUtils.loadTexture('images/building_plane_03.png', {}, function() {
      buildingMaterial03 = new THREE.MeshBasicMaterial(0xFFFFFF, 0);
      buildingMaterial03.map = buildingTexture03;
      buildingMaterial03.side = THREE.DoubleSide;
      buildingMaterial03.transparent = true;

      buildingContainer03 = new THREE.Object3D();

      var buildingGeom03 = new THREE.PlaneBufferGeometry(buildingPart03Width, buildingPart03Height),
          buildingPart03 = new THREE.Mesh(buildingGeom03, buildingMaterial03);
      buildingPart03.position.y = buildingPart03Height / 2;

      buildingContainer03.add(buildingPart03);
      buildingContainer03.position.x = 195;
      buildingContainer03.position.z = -21;
      buildingContainer03.rotation.y = -15 * Math.PI / 180;
      buildingGroup.add(buildingContainer03);

      // Init BuildingFigure to manipulate the whole group.
      building = new BuildingFigure(buildingGroup);
    });

    buildingGroup.position.z = 60;

    scene.add(buildingGroup);

    BuildingFigure = function(building) {
      this.container = building;
      this.isOpen = true;
      this.lockTransform = false;
      this.parts = {
        left: building.children[0] || buildingContainer01,
        center: building.children[1] || buildingContainer02,
        right: building.children[2] || buildingContainer03
      };
    };

    BuildingFigure.prototype.open = function(speed, callback) {
      if (this.lockTransform || this.isOpen) return;
      this.lockTransform = true;

      if (!speed) speed = DEFAULT_SPEED;

      var self = this;
      TweenMax.to(self.container.scale, speed / 2, { x: 1, y: 1, onComplete: function a() {
        TweenMax.to(self.container.rotation, speed, { delay: 0.1, x: 0 *Math.PI / 180 });
        TweenMax.to(self.parts.left.position, speed, { delay: 0.1, z: 8 });
        TweenMax.to(self.parts.left.rotation, speed, { delay: 0.1, y: -25 * Math.PI / 180 });
        TweenMax.to(self.parts.center.rotation, speed, { delay: 0.1, y: 15 * Math.PI / 180 });
        TweenMax.to(self.parts.right.position, speed, { delay: 0.1, z: -21, x: 195 });
        TweenMax.to(self.parts.right.rotation, speed, { delay: 0.1, y: -15 * Math.PI / 180, onComplete: function b() {
          self.lockTransform = false;
          self.isOpen = true;
          if (callback) callback();
        }});
      }});
    };

    BuildingFigure.prototype.close  = function(speed, callback) {
      if (this.lockTransform || !this.isOpen) return;
      this.lockTransform = true;

      if (!speed) speed = DEFAULT_SPEED;

      var targetValue01 = 0 * Math.PI / 180,
          targetValue02 = 89.5 *Math.PI / 180,
          self = this;

      TweenMax.to(this.parts.left.position, 0.05, { z: 0 });
      TweenMax.to(this.parts.left.rotation, 0.05, { y: targetValue01 });
      TweenMax.to(this.parts.center.position, 0.05, { z: 0 });
      TweenMax.to(this.parts.center.rotation, 0.05, { y: targetValue01 });
      TweenMax.to(this.parts.right.position, 0.05, { z: 0 });
      TweenMax.to(this.parts.right.rotation, 0.05, { y: targetValue01, onComplete: function a() {
        TweenMax.to(self.container.rotation, speed, { x: targetValue02, onComplete: function b() {
          TweenMax.to(self.container.scale, speed/2, { x: 0.8, y: 0.1 })
          self.lockTransform = false;
          self.isOpen = false;
          if (callback) callback();
        }});
      }});
    };

    // Character.
    characterTexture = new THREE.ImageUtils.loadTexture('images/girl_chariot.png', {}, function() {
      characterMaterial = new THREE.MeshBasicMaterial(0xFFFFFF, 0);
      characterMaterial.map = characterTexture;
      characterMaterial.side = THREE.DoubleSide;
      characterMaterial.transparent = true;

      characterGeom = new THREE.PlaneBufferGeometry(characterWidth, characterHeight);
      characterMesh = new THREE.Mesh(characterGeom, characterMaterial);
      characterContainer = new THREE.Object3D();
      characterContainer.add(characterMesh);
      characterContainer.position.x = 20;
      characterContainer.position.z = 195;
      characterMesh.position.y = characterHeight / 2;
      scene.add(characterContainer);

      character = new CharacterFigure(characterContainer);
    });

    CharacterFigure = function(character) {
      this.isOpen = true;
      this.lockTransform = false;
      this.container = character;
    };

    CharacterFigure.prototype.open = function(speed, callback) {
      if (this.lockTransform || this.isOpen) return;
      this.lockTransform = true;

      if (!speed) speed = DEFAULT_SPEED;

      var self = this;
      TweenMax.to(self.container.scale, speed, { y: 1, delay: speed, })
      TweenMax.to(self.container.rotation, speed, { x: 0 * Math.PI / 180, delay: speed, onComplete: function a() {
        self.lockTransform = false;
        self.isOpen = true;
        if (callback) callback();
      }});
    };

    CharacterFigure.prototype.close = function(speed, callback) {
      if (this.lockTransform || !this.isOpen) return;
      this.lockTransform = true;

      if (!speed) speed = DEFAULT_SPEED;

      var self = this;
      TweenMax.to(self.container.rotation, speed, { x: 90 * Math.PI / 180, onComplete: function a() {
        TweenMax.to(self.container.scale, speed, { y: 0.1 });
        self.lockTransform = false;
        self.isOpen = false;
        if (callback) callback();
      }});
    };

    // Light poles.
    lightPoleTexture = new THREE.ImageUtils.loadTexture('images/lamp.png', {}, function() {
      lightPoleMaterial = new THREE.MeshBasicMaterial(0xFFFFFF, 0)
      lightPoleMaterial.map = lightPoleTexture;
      lightPoleMaterial.side = THREE.DoubleSide;
      lightPoleMaterial.transparent = true;

      lightPoleGeom = new THREE.PlaneBufferGeometry(lightPoleWidth, lightPoleHeight);

      // 1 of 2.
      lightPole01Mesh = new THREE.Mesh(lightPoleGeom, lightPoleMaterial);
      lightPole01Container = new THREE.Object3D();
      lightPole01Container.add(lightPole01Mesh);
      lightPole01Mesh.position.y = lightPoleHeight / 2;
      lightPole01Container.position.x = 140;
      lightPole01Container.position.z = 140;
      scene.add(lightPole01Container);

      // 2 of 2.
      lightPole02Mesh = new THREE.Mesh(lightPoleGeom, lightPoleMaterial);
      lightPole02Container = new THREE.Object3D();
      lightPole02Container.add(lightPole02Mesh);
      lightPole02Mesh.position.y = lightPoleHeight / 2;
      lightPole02Container.position.x = -140;
      lightPole02Container.position.z = 140;
      scene.add(lightPole02Container);

      lightPoles = new LightPoles([lightPole01Container, lightPole02Container]);
    });

    LightPoles = function(lightPolesArray) {
      this.lightPoles = lightPolesArray;
      this.isOpen = true;
      this.lockTransform = false;
    };

    LightPoles.prototype.open = function(speed, callback) {
      if (this.lockTransform || this.isOpen) return;
      this.lockTransform = true;

      if (!speed) speed = DEFAULT_SPEED;

      var self = this;
      TweenMax.to(self.lightPoles[0].scale, speed, { y: 1, delay: 0.01 });
      TweenMax.to(self.lightPoles[0].rotation, speed, { x: 0 * Math.PI / 180, delay: 0.5 });
      TweenMax.to(self.lightPoles[1].scale, speed, { y: 1, delay: 0.01 });
      TweenMax.to(self.lightPoles[1].rotation, speed, { x: 0 * Math.PI / 180, delay: 0.5, onComplete: function a() {
        self.isOpen = true;
        self.lockTransform = false;
        if (callback) callback();
      }});
    };

    LightPoles.prototype.close = function(speed, callback) {
      if (this.lockTransform || !this.isOpen) return;
      this.lockTransform = true;

      if (!speed) speed = DEFAULT_SPEED;
      var self = this;
      TweenMax.to(self.lightPoles[0].scale, speed, { y: 0.1, delay: 0.1 });
      TweenMax.to(self.lightPoles[0].rotation, speed, { x: 90 * Math.PI / 180 });
      TweenMax.to(self.lightPoles[1].scale, speed, { y: 0.1, delay: 0.01 });
      TweenMax.to(self.lightPoles[1].rotation, speed, { x: 90 * Math.PI / 180, onComplete: function a() {
        self.isOpen = false;
        self.lockTransform = false;
        if (callback) callback();
      }});
    };

    scene.add(hemiLight);
    scene.add(ambientLight);

    controls = new THREE.OrbitControls(camera, renderer.domElement);

    $(window).on('keyup', function keyupHandler() {
      if (sleeves) {
        if (sleeves.isOpen) {
          sleeves.close(0.05);
        } else {
          sleeves.open(0.05);
        }
      }

      if (building) {
        if (building.isOpen) {
          building.close();
        } else {
          building.open();
        }
      }

      if (character) {
        if (character.isOpen) {
          character.close();
        } else {
          character.open();
        }
      }

      if (lightPoles) {
        if (lightPoles.isOpen) {
          lightPoles.close();
        } else {
          lightPoles.open();
        }
      }
    });
  }

  function animate(){
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    controls.update();
    stats.update();
  }

  function initStats() {
    var stats = new Stats();
    stats.setMode(0);
    $('#stats').append(stats.domElement);
    return stats;
  }
});
